# FTL Save Manager


## Name
FTL Save SCUMMing utility (Quick and dirty save state manager)

## Description
I created this utility because I got tired of manually copying and editing file names to save SCUMM in FTL. I may have gone farther than I initially intended. 


## Installation
Users will need to run the script in PowerShell as administrator. As with any script from the internet, I strongly encourage you to review it to confirm absence of malarky before running it on your machine. 

Upon first use it will prompt to create a subfolder in the FTL save folder. This subfolder is for managing the save backups without crapping up the main save folder. It is required for the script to work. If this part is giving you trouble or makes you nervous, you could manually create a folder titled "savescums" in your FTL save game folder. That will allow it to pass the check the first time without creating the folder via the script.

## Usage
When run in PowerShell as administrator, the script initializes functions to be called and managed with the command "FTL-Menu". When the FTL menu is called it presents the user with the following options:

"1. Quicksave to a numbered Backup File"

"2. Backup save file with custom name"

"3. Choose backup to restore from"

"4. Quick restore most recently made backup"

"5. Exit"

!!!NOTE!!! Please only restore your continue file backups while the game is at the main menu screen to avoid issues.

Options one through four will complete the corresponding action and then return you to the FTL-Menu. Selecting 5, or hitting ctrl+c will exit the menu.

As it stands the "Choose backup to restore from" will list the 15 most recent backups. I may tweak this value or break off a separate option to select from only quicksaves or named saves.

## Support
You can e-mail me at my user account @ gmail . com if you have any suggestions for improvements or are running into difficulties getting the script to work.

## Roadmap
I consider this "relatively" "done". Which means I'll probably poke and prod at it over the next few months as whimsy strikes. Happy to make feature additions upon recommendation.

## License
GNU GPL v 2 I guess?

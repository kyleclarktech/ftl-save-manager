#Checks to confirm save folder location exists and is writeable. If yes to both, checks for savescum subfolder and prompts for creation if not found.
function Check-FTLFolder {
    $folderPath = Join-Path $env:userprofile 'Documents\my games\fasterthanlight'
    if (!(Test-Path $folderPath)) {
        Write-Error "$folderPath does not exist"
        return $false
    }
    if (!(Get-Item $folderPath).psiscontainer) {
        Write-Error "$folderPath is not a folder"
        return $false
    }
    if (!(Get-Item $folderPath).Attributes.ToString().Contains('ReadOnly')) {
        Write-Host "$folderPath is writeable"
    } else {
        Write-Error "$folderPath is not writeable"
        return $false
    }
    $savescumsPath = Join-Path $folderPath 'savescums'
    if (!(Test-Path $savescumsPath)) {
        $createSavescums = Read-Host "It looks like this might be your first time running the save manager. The NEEDED subfolder for save management was not detected. Do you wish to create one in '$folderPath'? (Y/N)"
        if ($createSavescums.ToUpper() -eq 'Y') {
            New-Item -ItemType Directory -Path $savescumsPath | Out-Null
        } else {
            Write-Error "'savescums' subfolder does not exist in '$folderPath'"
            return $false
        }
    } else {
        Write-Host "'savescums' subfolder exists in '$folderPath'"
    }
    return $true
}
#Backup "continue.sav" file to a numbered version in the "savescums" subfolder. (quicksave)
function Backup-ContinueSav {
    $folderPath = Join-Path $env:userprofile 'Documents\my games\fasterthanlight'
    $savescumsPath = Join-Path $folderPath 'savescums'
    $continueSavPath = Join-Path $folderPath 'continue.sav'
    $backupCount = (Get-ChildItem $savescumsPath -Filter "continue.sav*" | Measure-Object).Count
    $backupPath = Join-Path $savescumsPath "continue.sav$($backupCount+1)"
    Copy-Item $continueSavPath $backupPath -Force
}
#Save backup of continue.sav as custom-name in savescums subfolder.
function Backup-ContinueSavCustom {
    $folderPath = Join-Path $env:userprofile 'Documents\my games\fasterthanlight'
    $savescumsPath = Join-Path $folderPath 'savescums'
    $continueSavPath = Join-Path $folderPath 'continue.sav'
    $backupName = Read-Host "Enter a name for the backup"
    $backupPath = Join-Path $savescumsPath "$backupName.sav"
    Copy-Item $continueSavPath $backupPath -Force
}
#List 15 most recent backups and restore user choice as the continue.sav file
function Restore-ContinueSav {
    $savesDir = "$env:userprofile\Documents\my games\fasterthanlight\savescums"

    if(!(Test-Path -Path $savesDir -PathType Container)) {
        Write-Host "No saved games found."
        return
    }

    $saves = Get-ChildItem $savesDir | Sort-Object LastWriteTime -Descending | Select-Object -First 15

    if($saves.Count -eq 0){
        Write-Host "No saved games found."
        return
    }

    Write-Host "Select a saved game to restore:"
    for($i = 0; $i -lt $saves.Count; $i++) {
        Write-Host "$i) $($saves[$i].Name)"
    }

    $selection = Read-Host "Enter the number of the saved game to restore"

    if(-not ([int]::TryParse($selection, [ref]$null))) {
        Write-Host "Invalid selection."
        return
    }

    $selection = [int]$selection

    if($selection -lt 0 -or $selection -ge $saves.Count) {
        Write-Host "Invalid selection."
        return
    }

    $selectedSave = $saves[$selection]

    $continueFile = "$env:userprofile\Documents\my games\fasterthanlight\continue.sav"
    Copy-Item -Path $selectedSave.FullName -Destination $continueFile -Force
    Write-Host "Game restored from $($selectedSave.Name)"
}
#Restore most recent backup (Quickload) 
function Restore-MostRecentCopy {
    $saveFolder = "$env:userprofile\Documents\my games\fasterthanlight\savescums"
    $mostRecentSave = Get-ChildItem -Path $saveFolder | Sort-Object -Property LastWriteTime -Descending | Select-Object -First 1
    if (-not $mostRecentSave) {
        Write-Output "No saved copies found."
        return
    }

    $fullPath = Join-Path -Path $saveFolder -ChildPath $mostRecentSave.Name
    $continuePath = Join-Path -Path $env:userprofile\"Documents\my games\fasterthanlight" -ChildPath "continue.sav"
    Copy-Item -Path $fullPath -Destination $continuePath -Force
    Write-Output "Most recent saved copy ('$($mostRecentSave.Name)') restored."
}

#Structure of the menu to call functions.
function FTL-Menu {
    Check-FTLFolder
    Clear-Host
    Write-Host "Please only restore your continue file backups while the game is at the main menu screen to avoid issues."
    Write-Host "1. Quicksave to a numbered Backup File"
    Write-Host "2. Backup save file with custom name"
    Write-Host "3. Choose backup to restore from"
    Write-Host "4. Quick restore most recently made backup"
    Write-Host "5. Exit"
    $selection = Read-Host "Enter the number of the operation you want to perform"
    switch ($selection) {
        '1' { Backup-ContinueSav }
        '2' { Backup-ContinueSavCustom }
        '3' { Restore-ContinueSav }
        '4' { Restore-MostRecentCopy }
        '5' { Exit }
        default { 
            Write-Warning "Invalid selection, please try again."
            FTL-Menu
        }
        
    }
    FTL-Menu
}
#Notifies user and prompts them to call the menu.
Write-Host "FTL-Menu functions initialized. You can now call the menu in this powershell session by typing FTL-Menu"